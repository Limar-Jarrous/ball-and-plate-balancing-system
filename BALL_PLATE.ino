#include <PID_v1.h>
#include <Arduino.h>
#include <Wire.h>

# include <DynamixelSerial1.h>
# include <DynamixelSerial2.h>
# define ID1 1
# define ID2 2

boolean start=false;
int Sx=0,Sy=0;
int rx=0,ry=0;

double SetpointX, InputX, OutputX;
double SetpointY, InputY, OutputY;
double ex,ey;
double ex_1,ey_1;
int speedm = 60;
int X_home = 2645;
int Y_home = 3456;
double Kp=20.3, Ki=0, Kd=9;
PID myPIDX(&InputX, &OutputX, &SetpointX, Kp, Ki, Kd, DIRECT);
PID myPIDY(&InputY, &OutputY, &SetpointY, Kp, Ki, Kd, DIRECT);
/*
float defP = 160, defI = 80, defD = 47, paramP = 0, paramI = 0, paramD = 0, paramPsc = 0, paramIsc = 0, paramDsc = 0;

float  K_wx = 0, K_wy = 0, s_1x = 0, s_1y = 0, y_x = 0, y_y = 0,yx = 0, yy = 0;*/

void setup() {
/*
  paramP = defP;
  paramD = defD;
  paramI = defI;
  paramPsc = paramP/100;
  paramIsc = paramI/100;
  paramDsc = paramD/100;
  */
  Dynamixel1.begin(1000000,2);
  Dynamixel2.begin(1000000,2);

  Serial.begin(9600);
  Serial3.begin(9600);

  Dynamixel1.moveSpeed(ID1,X_home,speedm);
  Dynamixel2.moveSpeed(ID2,Y_home,speedm);
  delay(500);

  myPIDX.SetMode(AUTOMATIC); // set output limits for PID controller
  myPIDX.SetOutputLimits(-780,780);
  myPIDX.SetSampleTime(10); // sample time for PID controller

  myPIDY.SetMode(AUTOMATIC);
  myPIDY.SetOutputLimits(-639,639);
  myPIDY.SetSampleTime(10);

  SetpointX = 0;
  SetpointY = 0;

}

void loop() {
  // put your main code here, to run repeatedly:
   Dynamixel1.moveSpeed(ID1,X_home,speedm);
   Dynamixel2.moveSpeed(ID2,Y_home,speedm);
   while(Serial3.available()){
      //Serial.println("seeing ball");
      char getData = Serial3.read();

      if (getData == '*') {
          rx = Serial3.parseInt();
          if (Serial3.read() == '@') {
              ry = Serial3.parseInt();
              process();
              //Serial.println("going to pro");
          }
      }
      if (getData == '#') {
          Kp=Serial2.parseInt();
          if (Serial2.read() == '*') {
              Ki =Serial2.parseInt();
              if (Serial2.read() == '&') {
                  Kd= Serial2.parseInt();
              }
          }
      }
  }
}


void process() {
  //Serial.println("fff");
  ex_1 = ex;
  ey_1 = ey;
  ex = SetpointX - rx;
  ey = SetpointY - ry;
  InputX = constrain(ex,-500,+500);
  InputY = constrain(ey,-500,+500);

  myPIDX.Compute();
  myPIDY.Compute();

  //   yy = rPID_poz(ey, ey_1, paramPsc, paramIsc, paramDsc, s_1y, K_wy);
  //   yx = rPID_poz(ex, ex_1, paramPsc, paramIsc, paramDsc, s_1x, K_wx);

  if ( InputX > 30 || InputX < -30 || InputY > 30 || InputY < -30 ) {

      Dynamixel1.moveSpeed(ID1,X_home+OutputX,speedm);
      Dynamixel2.moveSpeed(ID2,Y_home+OutputY,speedm);
  }
  else {
    Dynamixel1.moveSpeed(ID1,X_home,speedm);
    Dynamixel2.moveSpeed(ID2,Y_home,speedm);
    Serial.println("I'm at home");
  }
}
