import cv2
import numpy as np

#from picamera.array import PiRGBArray
#from picamera import PiCamera
import time

#camera = PiCamera()
#camera.resolution = (640, 480)
#camera.framerate = 30
#rawCapture = PiRGBArray(camera,size=(640,480))
cap = cv2.VideoCapture(1)
#optional argument
def nothing(x):
    pass

cv2.namedWindow('image')

#easy assigments
hh='HueH'
hl='HueL'
sh='SatH'
sl='SatL'
vh='ValH'
vl='ValL'

cv2.createTrackbar(hl, 'image',172,179,nothing)
cv2.createTrackbar(hh, 'image',180,180,nothing)
cv2.createTrackbar(sl, 'image',86,255,nothing)
cv2.createTrackbar(sh, 'image',255,255,nothing)
cv2.createTrackbar(vl, 'image',127,255,nothing)
cv2.createTrackbar(vh, 'image',255,255,nothing)

time.sleep(2.0)
while True:
    _, frame = cap.read()
    hsv=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    rows,cols=frame.shape[:2]
    #M = cv2.getRotationMatrix2D((cols/2,rows/2),180,1)
    #frame = cv2.warpAffine(frame,M,(cols,rows))
    #read trackbar positions for all
    hul=cv2.getTrackbarPos(hl, 'image')
    huh=cv2.getTrackbarPos(hh, 'image')
    sal=cv2.getTrackbarPos(sl, 'image')
    sah=cv2.getTrackbarPos(sh, 'image')
    val=cv2.getTrackbarPos(vl, 'image')
    vah=cv2.getTrackbarPos(vh, 'image')
    #make array for final values
    HSVLOW=np.array([hul,sal,val])
    HSVHIGH=np.array([huh,sah,vah])

    #apply the range on a mask
    mask = cv2.inRange(hsv,HSVLOW, HSVHIGH)
    mask2 = cv2.GaussianBlur(mask, (7, 7), 0)

    kernel = np.ones((7, 7), np.uint8)
    mask = cv2.erode(mask, kernel, iterations=1)
    mask = cv2.dilate(mask, kernel, iterations=1)
    
    cv2.imshow('image', mask2)
    cv2.imshow('frame', frame)
    #rawCapture.truncate(0)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break


cv2.destroyAllWindows()
