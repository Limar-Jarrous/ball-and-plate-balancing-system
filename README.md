# Ball and Plate Balancing System (Graduation Project)

## The Main Goal
The ball and plate system is an upgraded version of the ball and beam system where the position of the ball can be manipulated in two directions (x and y axes).
The general control purpose in this project is to stabilize the position of the ball at the center of the plate using a camera to find the current position of the ball and two PID controllers to calculate the errors, which are the distances between the ball position and the center point over the two axes.

## Image Processing
for image processing, We used Python with OpenCV library to detect the position of the ball by capturing an image from the camera and convert it to HSV colorspace, so we can avoid lightning issues. Next, we did some filtering and morphological operations on the image to get masks of the ball and plate objects. Then, we found the centers of these objects and sent them to the Arduino, which will change the plate's angles using two servo motors.

## Dynamixel Motors
When Arduino receives the position of the ball via buetooth device, the PIDs calculate the errors. After that, we convert the output to servo's angles. Then, we change our Dynamixel motors' angles, using DynamixelSerial library, to correct the position of the ball.

Note that in our case we need to control two servos at the same time, so we have to use two DynamixelSerial libraries. Therefore, I made a copies of the library and changed them to control our servos. you can find the libraries in "DynamixelSerial.zip".
