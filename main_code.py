# Import Libraries
import cv2
import numpy as np
import serial
from time import sleep

x = 0
y = 0

# Capture Video
cap = cv2.VideoCapture(1)

ser = serial.Serial('COM10', 9600, timeout=0.5, \
            parity=serial.PARITY_EVEN, rtscts=1)

while(1):

    # Take each frame
    _ , img = cap.read()
    #cv2.imshow('Image',img)

    # Convert BGR to HSV
    img = cv2.GaussianBlur(img, (7, 7), 0)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


    # define range of blue color in HSV
    lowerGreen= (76,229,0)
    upperGreen=(100,255,64)
    lowerOrange= (16,30,172)
    upperOrange=(44,207,255)
    lowerOrange1= (14,79,162)
    upperOrange1=(34,170,255)
    lowerRedBox=(0,0,49)
    upperRedBox=(180,131,53)
    lowerRedBox1=(0,63,54)
    upperRedBox1=(180,173,144)

    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lowerOrange, upperOrange)
    mask = cv2.GaussianBlur(mask, (7, 7), 0)
    kernel = np.ones((7, 7), np.uint8)
    mask = cv2.erode(mask, kernel, iterations=2)
    mask = cv2.dilate(mask, kernel, iterations=2)
    mask2 = cv2.inRange(hsv, lowerRedBox1, upperRedBox1)
    mask2 = cv2.GaussianBlur(mask2, (7, 7), 0)
    mask2 = cv2.erode(mask2, kernel, iterations=2)
    mask2 = cv2.dilate(mask2, kernel, iterations=2)
    #cv2.imshow('Mask',mask)


    # Finding Contours
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    cnts2 = cv2.findContours(mask2.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = (0,0)
    

    if len(cnts) > 0 :

        #cnt = contours[0]
        c = max(cnts, key=cv2.contourArea)

        area = cv2.contourArea(c)
        print(area)
        if (area > 650) & (area < 3000) :
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            # only proceed if the radius meets a minimum size
            if radius > 10:
                # draw the circle and centroid on the frame,
                # then update the list of tracked points
                cv2.circle(img, (int(x), int(y)), int(radius),(0, 255, 255), 2)
                cv2.circle(img,(int(x), int(y)) , 3, (0, 0, 255), -1)


    if len(cnts2) > 0:

        c2 = max(cnts2, key=cv2.contourArea)
        epsilon = 0.1 * cv2.arcLength(c2, True)
        approx = cv2.approxPolyDP(c2, epsilon, True)


        x1, y1, w1, h1 = cv2.boundingRect(approx)
        ccc0 = (x1, y1)
        ccc2 = (x1 + w1, y1 + h1)

        cv2.drawContours(img, [approx], -1, (0, 255, 0), 3)
        M2 = cv2.moments(c2)
        center2 = (int(M2["m10"] / M2["m00"]), int(M2["m01"] / M2["m00"]))
        cv2.drawMarker(img, center2, (255, 0, 0))

        if abs(center[0] > 0):
            Xp = int(1000 * ((center[0] - ccc0[0]) / (ccc2[0] - ccc0[0])) - 500)
            Yp = int(-1000 * ((center[1] - ccc0[1]) / (ccc2[1] - ccc0[1])) + 500)

            #command = "@%4d&%4d*" % (Xp, Yp)
            command = "*%s@%s" % (Xp, Yp)
            ser.write(command.encode())
            print(str(Xp) + ' , ' + str(Yp))


    sleep(0.01)

    cv2.imshow('ball',img)
    #cv2.imshow('plate',mask2)

    k = cv2.waitKey(1)
    if k == 27:
        break


print('close')
ser.close()

cap.release()
cv2.destroyAllWindows()
